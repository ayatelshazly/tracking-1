﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TrackingVehiclesTask.BLL;
using TrackingVehiclesTask.BLL.DTO;

namespace TrackingVehiclesTask.Api.Controllers
{
    public class CustomerController : ApiController
    {
        protected readonly VehicleTrackingBusiness _vehicleTrackingBusiness;

        public CustomerController()
        {
            _vehicleTrackingBusiness = new VehicleTrackingBusiness();
        }

        // GET api/values
        public IEnumerable<CustomerDTO> Get()
        {
            var cusomersList = _vehicleTrackingBusiness.GetCustomers();
            return cusomersList;
        }



    }
}